from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:activity_id>/', views.get_activity_byId, name='activity'),
    path('users', views.get_user, name='user')
]
