from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse

from .models  import User, Activity

# Create your views here.

def index(request):
    return HttpResponse("First words from Django")

def get_activity_byId(request, act_id):
    activity = get_object_or_404(Activity, pk=act_id)
    return JsonResponse(activity)

def get_user(request):
    return JsonResponse(User.objects.all())
