from django.db import models

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    nick_name = models.CharField(max_length=10)

    def __str__(self):
        return self.first_name
    
    @property
    def full_name(self):
        return '%s, %s'%(self.first_name, self.last_name)
    
    def nickName(self):
        return self.nick_name


class Activity(models.Model):
    
    "Activity choices"
    ACTIVITY_TYPES = [
        ('SWIM', 'Swim'),
        ('BIKE', 'Bike'),
        ('RUN', 'Run'),
        ('BRICK', 'Brick'),
        ('GYM', 'Gym')
    ]

    "Activity Types for choices"
    ACTIVITY_MODES = [
        ('E', 'Easy'),
        ('T', 'Tempo'),
        ('H', 'Max Effort'),
        ('I', 'Interval'),
        ('L', 'Long')
    ]

    length = models.DecimalField(max_digits=4, decimal_places=2)
    activity_types = models.CharField(max_length=5, choices=ACTIVITY_TYPES)
    activity_modes = models.CharField(max_length=1, choices=ACTIVITY_MODES)
    comment = models.TextField(max_length=400)
    activity_date = models.DateField()
    completed = models.BooleanField(default=False)


    def __str__(self):
        return self.activity_types
    
    def activity_description(self):
        return ('%s', '%s', '%s', '%s')%(self.activity_types, self.activity_modes, self.length, "{:%d/%m/%Y}".format(self.activity_date))

