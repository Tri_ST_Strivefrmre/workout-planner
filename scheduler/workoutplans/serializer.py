from rest_framework import serializers
from .models import Activity, User

class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('length', 'activity_types', 'activity_modes','comment','activity_date','completed')

